package com.tadahtech.kitpvpstats.sql;

import com.tadahtech.kitpvpstats.KitPvPStats;
import com.tadahtech.kitpvpstats.player.PlayerInfo;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import java.sql.*;
import java.util.UUID;

/**
 * Manages all interactions with a MySQL database
 */
public class SQLManager {

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS `kitpvp_stats` (`uuid` VARCHAR(64) PRIMARY KEY NOT NULL, `kills` INTEGER, `deaths` INTEGER, `damage_dealt` DOUBLE);";
    private static final String CREATE_NAMES = "CREATE TABLE IF NOT EXISTS `player_names` (`uuid` VARCHAR(64) PRIMARY KEY NOT NULL, `name` VARCHAR(16));";

    private static final String GET_PLAYER = "SELECT * FROM `kitpvp_stats` WHERE `uuid` = ?";
    private static final String UPSERT_PLAYER = "INSERT INTO `kitpvp_stats` VALUES(?,?,?,?) ON DUPLICATE KEY UPDATE `kills` = ?, `deaths` = ?, `damage_dealt` = ?";

    private static final String UPDATE_NAME = "INSERT INTO `player_names` VALUES(?,?) ON DUPLICATE KEY UPDATE `name` = ?";
    private static final String GET_STATS_FROM_NAME = "SELECT kitpvp_stats.* FROM player_names JOIN kitpvp_stats ON player_names.uuid = kitpvp_stats.uuid WHERE player_names.name = ?";

    private final KitPvPStats plugin;

    private final HikariDataSource dataSource;

    public SQLManager() {
        this.plugin = KitPvPStats.getInstance();
        FileConfiguration config = plugin.getConfig();

        ConfigurationSection sql = config.getConfigurationSection("sql");

        String host = sql.getString("host");
        int port = sql.getInt("port", 3306);
        String database = sql.getString("database");
        String user = sql.getString("user");
        String password = sql.getString("password");

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:mysql://" + host + ":" + port + "/" + database);
        hikariConfig.setDriverClassName("com.mysql.jdbc.Driver");
        hikariConfig.setUsername(user);
        hikariConfig.setPassword(password);
        hikariConfig.setMinimumIdle(1);
        hikariConfig.setMaximumPoolSize(10);
        hikariConfig.setConnectionTimeout(10000);

        this.dataSource = new HikariDataSource(hikariConfig);
        runAsync(() -> {
            try (Connection connection = getConnection()) {
                Statement statement = connection.createStatement();
                statement.execute(CREATE_TABLE);
                statement.execute(CREATE_NAMES);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Updates a player's name -> UUID mapping the DB for easier offline player stats lookup
     *
     * @param player The uuid of the player
     * @param name   The name of the player
     */
    public void updateName(UUID player, String name) {
        runAsync(() -> {
            try (Connection connection = getConnection()) {
                new SQLStatement(UPDATE_NAME).set(player).set(name).set(name).prepare(connection).execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Returns the stats for an offline user, using his name
     *
     * @param name     The player's name
     * @param callback The container for the player's stats
     */
    public void getOfflinePlayerStats(String name, Callback<PlayerInfo> callback) {
        runAsync(() -> {
            SQLStatement statement = new SQLStatement(GET_STATS_FROM_NAME).set(name);

            try (Connection connection = getConnection()) {

                ResultSet resultSet = getResultSet(statement, connection);

                if (resultSet == null || !resultSet.next()) {
                    runSync(() -> callback.call(null));
                    return;
                }

                int kills = resultSet.getInt("kills");
                int deaths = resultSet.getInt("deaths");
                double damageDealt = resultSet.getDouble("damage_dealt");

                runSync(() -> {
                    PlayerInfo playerInfo = new PlayerInfo(kills, deaths, damageDealt);
                    callback.call(playerInfo);
                });
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Loads a player's information from the database, providing he is in it, using his UUID
     * In the case where the player is not, it will create a new instance of {@link PlayerInfo} with fresh stats.
     * This is run on a separate thread, and the result is synced via a {@link Callback<PlayerInfo>} onto the main thread
     *
     * @param uuid     The player's uuid
     * @param callback The callback containing the new {@link PlayerInfo} class for the specific player
     */
    public void loadPlayer(UUID uuid, Callback<PlayerInfo> callback) {
        runAsync(() -> {
            SQLStatement statement = new SQLStatement(GET_PLAYER).set(uuid);

            try (Connection connection = getConnection()) {

                ResultSet resultSet = getResultSet(statement, connection);

                if (resultSet == null || !resultSet.next()) {
                    runSync(() -> callback.call(new PlayerInfo(uuid)));
                    return;
                }

                int kills = resultSet.getInt("kills");
                int deaths = resultSet.getInt("deaths");
                double damageDealt = resultSet.getDouble("damage_dealt");

                runSync(() -> callback.call(new PlayerInfo(uuid, kills, deaths, damageDealt)));

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Saves a player's stats into SQL
     *
     * @param info The info of the player being saved
     */
    public void savePlayer(PlayerInfo info) {
        runAsync(() -> {
            SQLStatement statement = new SQLStatement(UPSERT_PLAYER).set(info.getUuid())
              .set(info.getKills())
              .set(info.getDeaths())
              .set(info.getDamageDealt())
              //Repeat for UPSERT
              .set(info.getKills())
              .set(info.getDeaths())
              .set(info.getDamageDealt());

            try (Connection connection = getConnection()) {
                statement.prepare(connection).execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        });
    }

    private Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    private ResultSet getResultSet(SQLStatement query, Connection connection) {
        try {
            PreparedStatement preparedStatement = query.prepare(connection);
            preparedStatement.execute();
            return preparedStatement.getResultSet();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void runAsync(Runnable runnable) {
        this.plugin.getServer().getScheduler().runTaskAsynchronously(plugin, runnable);
    }

    private void runSync(Runnable runnable) {
        this.plugin.getServer().getScheduler().runTask(plugin, runnable);
    }

}
