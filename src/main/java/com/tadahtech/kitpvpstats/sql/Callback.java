package com.tadahtech.kitpvpstats.sql;

/**
 * Class used for returning results from an async thread
 * @param <T> The object
 */
public interface Callback<T>
{
    void call(T t);

}
