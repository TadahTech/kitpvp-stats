package com.tadahtech.kitpvpstats;

import com.tadahtech.kitpvpstats.command.StatsCommand;
import com.tadahtech.kitpvpstats.listener.PlayerListener;
import com.tadahtech.kitpvpstats.sql.SQLManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The main class for {@link KitPvPStats}
 */
public class KitPvPStats extends JavaPlugin {

    private static KitPvPStats instance;
    private SQLManager sqlManager;

    @Override
    public void onEnable() {
        instance = this;
        this.sqlManager = new SQLManager();
        new PlayerListener(this);
        getCommand("stats").setExecutor(new StatsCommand(this.sqlManager));
        getLogger().info("Tracking KitPvP-Stats!");
    }

    public static KitPvPStats getInstance() {
        return instance;
    }

    public SQLManager getSqlManager() {
        return sqlManager;
    }
}
