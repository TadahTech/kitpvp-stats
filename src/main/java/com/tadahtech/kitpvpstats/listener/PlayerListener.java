package com.tadahtech.kitpvpstats.listener;

import com.tadahtech.kitpvpstats.KitPvPStats;
import com.tadahtech.kitpvpstats.player.PlayerInfo;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;

/**
 * Handles loading/saving/incrementing a Player's stats
 */
public class PlayerListener implements Listener {

    private KitPvPStats plugin;

    public PlayerListener(KitPvPStats plugin) {
        this.plugin = plugin;
        this.plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        UUID player = event.getPlayer().getUniqueId();
        this.plugin.getSqlManager().loadPlayer(player, info -> {});
        this.plugin.getSqlManager().updateName(player, event.getPlayer().getName());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        PlayerInfo playerInfo = PlayerInfo.clean(event.getPlayer().getUniqueId());
        this.plugin.getSqlManager().savePlayer(playerInfo);
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();

        if(player.getKiller() == null) {
            return;
        }

        Player killer = player.getKiller();

        PlayerInfo playerInfo = PlayerInfo.get(player);
        PlayerInfo killerInfo = PlayerInfo.get(killer);

        if(playerInfo != null) {
            playerInfo.incrementDeaths();
        }

        if(killerInfo != null) {
            killerInfo.incrementKills();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onDamage(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();
        Entity damagingEntity = event.getDamager();

        if(!(damagingEntity instanceof Player) && !(entity instanceof Player)) {
            return;
        }

        if(event.isCancelled()) {
            return;
        }

        Player player = (Player) damagingEntity;

        PlayerInfo playerInfo = PlayerInfo.get(player);

        double finalDamage = event.getFinalDamage();

        if(playerInfo == null) {
            this.plugin.getSqlManager().loadPlayer(player.getUniqueId(), info -> info.addDamageDealt(finalDamage));
            return;
        }

        playerInfo.addDamageDealt(finalDamage);
    }

}
