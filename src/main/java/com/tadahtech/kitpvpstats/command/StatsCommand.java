package com.tadahtech.kitpvpstats.command;

import com.tadahtech.kitpvpstats.player.PlayerInfo;
import com.tadahtech.kitpvpstats.sql.SQLManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

/**
 * Command which displays a users stats, or a target player
 */
public class StatsCommand implements CommandExecutor {

    private final SQLManager sqlManager;
    
    public StatsCommand(SQLManager sqlManager) {
        this.sqlManager = sqlManager;
    }
    
    private final String LINE = ChatColor.DARK_AQUA.toString() + ChatColor.BOLD + ChatColor.STRIKETHROUGH + "===============[ " + ChatColor.WHITE + ChatColor.RESET + "KitPvP Stats" + ChatColor.DARK_AQUA + ChatColor.BOLD + ChatColor.STRIKETHROUGH + " ]===============";
    private final DecimalFormat FORMAT = new DecimalFormat("##.##");

    @Override
    public boolean onCommand(CommandSender sender, Command command, String aliasUsed, String[] args) {
        if(sender instanceof ConsoleCommandSender && args.length == 0) {
            sender.sendMessage(ChatColor.RED + "The console doesn't have any stats!");
            return true;
        }

        if(args.length == 0) {
            Player player = (Player) sender;

            PlayerInfo info = PlayerInfo.get(player);

            if(info == null) {
                this.sqlManager.loadPlayer(player.getUniqueId(), playerInfo -> sendStats(player, playerInfo));
                return true;
            }
            
            sendStats(player, info);
            return true;
        }
        
        String target = args[0];
        
        Player playerTarget = Bukkit.getPlayerExact(target);
        
        if(playerTarget == null) {
            this.sqlManager.getOfflinePlayerStats(target, info -> {
                
                if(info == null) {
                    sender.sendMessage(ChatColor.RED + "Could not find " + ChatColor.YELLOW + target + "'s" + ChatColor.RED + " stats");
                    return;
                }
                
                sendStats(sender, info);
            });
            return true;
        }
        
        PlayerInfo info = PlayerInfo.get(playerTarget);
        
        if(info == null) {
            this.sqlManager.loadPlayer(playerTarget.getUniqueId(), playerTargetInfo -> sendStats(sender, playerTargetInfo));
            return true;
        }
        
        sendStats(sender, info);

        return true;
    }

    private void sendStats(CommandSender sender, PlayerInfo info) {
        sender.sendMessage(LINE);
        sender.sendMessage(" ");
        sender.sendMessage(format("Kills", info.getKills()));
        sender.sendMessage(format("Deaths", info.getDeaths()));
        sender.sendMessage(ChatColor.GREEN + "Damage Dealt" + ChatColor.GRAY + " - " + ChatColor.WHITE + FORMAT.format(info.getDamageDealt()));
        sender.sendMessage(" ");
        sender.sendMessage(LINE);
    }

    private String format(String skill, int value) {
        return ChatColor.GREEN + skill + ChatColor.GRAY + " - " + ChatColor.WHITE + value;
    }
}
