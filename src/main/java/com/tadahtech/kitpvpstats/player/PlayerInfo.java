package com.tadahtech.kitpvpstats.player;

import com.google.common.collect.Maps;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.UUID;

/**
 * A wrapper for storing a Player's stats
 * This allows us to modify stats in real time memory, without having to update MySQL constantly
 */
public class PlayerInfo {

    private static final Map<UUID, PlayerInfo> INFO_MAP = Maps.newHashMap();

    private final UUID uuid;

    private Player player;
    private int kills;
    private int deaths;
    private double damageDealt;

    public PlayerInfo(UUID uuid, int kills, int deaths, double damageDealt) {
        this.uuid = uuid;
        this.kills = kills;
        this.deaths = deaths;
        this.damageDealt = damageDealt;
        INFO_MAP.put(this.uuid, this);
    }

    public PlayerInfo(UUID uuid) {
        this(uuid, 0, 0, 0);
    }

    public PlayerInfo(int kills, int deaths, double damageDealt) {
        this.uuid = null;
        this.kills = kills;
        this.deaths = deaths;
        this.damageDealt = damageDealt;
    }

    public static PlayerInfo get(UUID uuid) {
        return INFO_MAP.get(uuid);
    }

    public static PlayerInfo get(Player player) {
        return get(player.getUniqueId());
    }

    public static PlayerInfo clean(UUID uuid) {
        return INFO_MAP.remove(uuid);
    }

    public UUID getUuid() {
        return uuid;
    }

    public Player getPlayer() {
        if(player == null) {
            this.player = Bukkit.getPlayer(this.uuid);
        }

        return player;
    }

    public int getKills() {
        return kills;
    }

    public void addKills(int kills) {
        this.kills += kills;
    }

    public void incrementKills() {
        this.addKills(1);
    }

    public int getDeaths() {
        return deaths;
    }

    public void addDeaths(int deaths) {
        this.deaths += deaths;
    }

    public void incrementDeaths() {
        this.addDeaths(1);
    }

    public double getDamageDealt() {
        return damageDealt;
    }

    public void addDamageDealt(double damageDealt) {
        this.damageDealt += damageDealt;
    }



}
